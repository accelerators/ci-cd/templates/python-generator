import os
import sys
import pathlib
import re


####################################################
# FUNCTION

def parse_env2list(str_var):
    '''
    Build list from variables
    '''
    lst_output = []
    separators = [':',';', ',']
    for separator in separators:
        if separator in str_var:
            lst_output = str_var.split(separator)
            break
    if not lst_output:
        lst_output = [str_var]
    return lst_output


def valid_items(lst_input, lst_valid_items):
    '''
    Return list with only validate OS, Tango version...
    '''
    lst_output = []
    if not isinstance(lst_input, list):
        lst_input = [lst_input]
    for item in lst_input:
        if item in lst_valid_items:
            lst_output.append(item)
    return lst_output


def update_var_os(key, data):
  data = data.replace('<OS_LABEL>', key)
  data = data.replace('<OS_DOCKER_IMAGE>', os_map_docker_image[key])
  data = data.replace('<OS_DIR>', os_map_dir[key])
  data = data.replace('<OS_CXX_STANDARD>', os_map_cxx[key])
  return data


def update_var_tango(key, data):
  data = data.replace('<TANGO_LABEL>', tango_map_label[key])
  data = data.replace('<TANGO_VERS>', tango_map_dir[key])
  return data


def update_var_build(key, data):
  data = data.replace('<BUILD_LABEL>', key)
  data = data.replace('<BUILD_TYPE>', build_map_cmake[key])
  data = data.replace('<BUILD_TYPE_DIR>', build_map_dir[key])
  return data



####################################################
# GLOABL VAR


# mapping values
os_map_docker_image = {
    'deb9': 'build-tools-image/debian:9-build',
    'ub20': 'build-tools-image/ubuntu:20.04-build',
    'ub22': 'build-tools-image/ubuntu:22.04-build',
    'ub24': 'build-tools-image/ubuntu:24.04-build',
}
os_map_dir = {
    'deb9': 'debian9',
    'ub20': 'ubuntu20.04',
    'ub22': 'ubuntu22.04',
    'ub24': 'ubuntu24.04',
}
os_map_cxx = {
    'deb9': '14',
    'ub20': '14',
    'ub22': '17',
    'ub24': '17',
}

tango_map_dir = {
    '9.3': 'tango-9.3',
    '9.4': 'tango-9.4',
    '9.5': 'tango-9.5',
    '10.0': 'tango-10.0',
}
tango_map_label = {
    '9.3': 't93',
    '9.4': 't94',
    '9.5': 't95',
    '10.0': 't100',
}

build_map_cmake = {
    'deb': 'Debug',
    'rel': 'Release',
    'rwd': 'RelWithDebInfo',
    'msr': 'MinSizeRel',
}
build_map_dir = {
    'deb': 'debug',
    'rel': 'release',
    'rwd': 'release',
    'msr': 'release',
}

# Couple OS/TANGO ESRF supported (Tango 10.xx need CXX17 only configure for Ub22 and upper)

tango_allowed = {
    'deb9': ['9.3', '9.5'],
    'ub20': ['9.3', '9.5', '10.0'],
    'ub22': ['9.3', '9.5', '10.0'],
    'ub24': ['9.3', '9.5', '10.0'],
}



####################################################
# BEGIN
print('\n###############################################')
print('START YAML GENERATOR SCRIPT')

# Parse environment variable to get user pipeline selection for : OS. BUILD type and TANGO version
GITLAB_CI_VAR = {}
for item in ['CPP_OS', 'CPP_TANGO']:
    
    print('\n# Check {} options'.format(item.split('_')[1]))
    # use Dic to store var contents
    GITLAB_CI_VAR[item] = {}
    
    # Get XXX_ALL (Gitlab 'Accelerators' CI variable) items and extract them to list
    sys_env = os.getenv(item + '_ALL')
    if sys_env is None:
        msg = ' - Fail to get Gitlab variable {}_ALL'.format(item)
        sys.exit(msg)
    else:
        GITLAB_CI_VAR[item]['ALL'] = parse_env2list(sys_env)
        print(' - Available {}: {}'.format(item, GITLAB_CI_VAR[item]['ALL']))
    
    # Get XXX_DEFAULT (Gitlab 'Accelerators' CI variable) items and extract them to list
    sys_env = os.getenv(item + '_DEFAULT')
    if sys_env is None:
        msg = ' - Fail to get Gitlab variable {}_DEFAULT'.format(item)
        sys.exit(msg)
    else:
        GITLAB_CI_VAR[item]['DEFAULT'] = parse_env2list(sys_env)
        print(' - Default {}: {}'.format(item, GITLAB_CI_VAR[item]['DEFAULT']))
    
    # Define requested item from user or from predefined var
    sys_env = os.getenv(item)
    print(' - Variable {} set with value {}'.format(item, sys_env))
    if sys_env is None or sys_env.lower() == 'default':
        GITLAB_CI_VAR[item]['REQUESTED'] = GITLAB_CI_VAR[item]['DEFAULT']
        print('   - Default items used for {}: {}'.format(item, GITLAB_CI_VAR[item]['REQUESTED']))
    elif sys_env.lower() == 'all':
        GITLAB_CI_VAR[item]['REQUESTED'] = GITLAB_CI_VAR[item]['ALL']
        print('   - All items selected for {}: {}'.format(item, GITLAB_CI_VAR[item]['REQUESTED']))
    else:
        sys_env_lst = parse_env2list(sys_env)
        GITLAB_CI_VAR[item]['REQUESTED'] = valid_items(sys_env_lst, GITLAB_CI_VAR[item]['ALL'])
        print('   -Specific items selected for {}: {}'.format(item, GITLAB_CI_VAR[item]['REQUESTED']))

        

# Get Envirronment variables which define job to create [built, test, deploy] with rules [manual, auto]
VAR_RULES_LST = ['BUILD_MANUAL', 'BUILD_AUTO', 
                 'TEST_MANUAL', 'TEST_AUTO', 
                 'DEPLOY_MANUAL', 'DEPLOY_AUTO', 'DEPLOY_FORCE', 
                 'CI_PIPELINE_SOURCE', 'CI_COMMIT_TAG']

version_pattern = r'^\d+.\d+.\d+'
version_regexp = re.compile(version_pattern)

# Set default values
BUILD = TEST = DEPLOY = False
CI_MR = CI_TAG = False
FORCE = False
MANUAL = True

print('\n# Check gitlab pipeline variables')
for var_name in VAR_RULES_LST:
    var_value = os.getenv(var_name)
    if var_value is not None:
        
        print(' - Variable {} set with value {}'.format(var_name, var_value))
        
        if var_name[:5] == 'BUILD':
            BUILD = True
        if var_name[:4] == 'TEST':
            TEST = True
        if var_name[:6] == 'DEPLOY':
            DEPLOY = True
        
        if var_name[-4:] == 'AUTO':
            MANUAL = False
            
        if var_name[-5:] == 'FORCE':
            FORCE = True
        
        if var_name == 'CI_PIPELINE_SOURCE' and var_value == 'merge_request_event':
            CI_MR = True
            MANUAL = False
        if var_name == 'CI_COMMIT_TAG' and version_regexp.match(var_value):
            CI_TAG = True
            MANUAL = False

print('\n# Configuration variables:')
print(' - {} = {}'.format('BUILD', BUILD))
print(' - {} = {}'.format('TEST', TEST))
print(' - {} = {}'.format('DEPLOY', DEPLOY))
print(' - {} = {}'.format('MANUAL', MANUAL))
print(' - {} = {}'.format('FORCE', FORCE))
print(' - {} = {}'.format('CI_MR', CI_MR))
print(' - {} = {}'.format('CI_TAG', CI_TAG))



# Depending previous variable detected, define Jobs to execute in child pipelines

if MANUAL:
    _when = 'manual'
else:
    _when = 'always'


###########################################################################
# Build output yaml and replace some labels by selected : OS, BUILD type or TANGO version

# Create root Yaml file
output_data = pathlib.Path('./templates/base.yml').read_text()

# Add anonymous job which allow entry point for .user-ci.yml

template_os = pathlib.Path('./templates/os-common.yml').read_text()
for _os in GITLAB_CI_VAR['CPP_OS']['REQUESTED']:
    output_data += update_var_os(_os, template_os)

template_tango = pathlib.Path('./templates/tango-common.yml').read_text()
for _tango in GITLAB_CI_VAR['CPP_TANGO']['REQUESTED']:
    output_data += update_var_tango(_tango, template_tango)

# Select template type depending pipeline requested
if CI_TAG or (DEPLOY and CI_TAG) or FORCE:
    job_template = pathlib.Path('./templates/workflow-deploy-tango.yml').read_text()

elif CI_MR or TEST:
    job_template = pathlib.Path('./templates/workflow-test-tango.yml').read_text()

elif BUILD:
    job_template = pathlib.Path('./templates/workflow-build-tango.yml').read_text()

else:
    msg = '\nNo variable found, to create job, from: {}'.format(VAR_RULES_LST)
    sys.exit(msg)

for _os in GITLAB_CI_VAR['CPP_OS']['REQUESTED']:

    for _build in ['deb', 'rwd']:

        for _tango in GITLAB_CI_VAR['CPP_TANGO']['REQUESTED']:

            # Apply filter to skip cppTango version not allow depending OS
            if _tango in tango_allowed[_os]:

                data = job_template
                data = update_var_os(_os, data)
                data = update_var_build(_build, data)
                data = update_var_tango(_tango, data)
                data = data.replace('<WHEN>', _when)
                output_data += data
            
            else:
                print(" tango {} not allow with OS {}, configuration skipped !".format(_tango, _os))



###########################################################################

if CI_TAG or (DEPLOY and CI_TAG) or FORCE:
    template_ci_release = pathlib.Path('./templates/job-release.yml').read_text()
    output_data += template_ci_release

user_ci_file = '.user-ci.yml'
if os.path.isfile(user_ci_file):
    print('  -  Add specific user_ci_file')
    output_data += 'include:\n  - "{}"'.format(user_ci_file)


yml_dir = 'job'
yml_file = 'child-ci.yml'
yml_path = '{}/{}'.format(yml_dir, yml_file)

mode = 0o666
os.mkdir(yml_dir, mode)
root_file = open(yml_path, 'x')
root_file.writelines(output_data)
root_file.close()
